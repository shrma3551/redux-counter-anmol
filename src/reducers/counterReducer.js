const initialState = {
  count: 0
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case "INCREMENT_COUNTER":
      state = {
        ...state,
        count: state.count + 1
      };
      break;
    case "DECREMENT_COUNTER":
      state = {
        ...state,
        count: state.count - 1
      };
      break;
    default:
      return state;
  }
  return state;
};

export default counterReducer;

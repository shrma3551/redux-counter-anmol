import { INCREMENT_COUNTER } from "./types";

export const incrementCounter = () => dispatch => {
  type: INCREMENT_COUNTER;
};

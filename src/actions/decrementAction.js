import { DECREMENT_COUNTER } from "./types";

export const decrementCounter = () => dispatch => {
  type: DECREMENT_COUNTER;
};

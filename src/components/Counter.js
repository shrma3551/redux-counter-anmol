import React from "react";
import { connect } from "react-redux";
const Counter = props => {
  return (
    <div>
      <button onClick={props.decrementCounter}>-</button>
      <span>{props.count}</span>
      <button onClick={props.incrementCounter}>+</button>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    count: state.count
  };
};

const mapDispatchToProps = dispatch => {
  return {
    incrementCounter: () => {
      dispatch({
        type: "INCREMENT_COUNTER"
      });
    },

    decrementCounter: () => {
      dispatch({
        type: "DECREMENT_COUNTER"
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
